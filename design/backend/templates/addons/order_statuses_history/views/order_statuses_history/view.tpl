{capture name="mainbox"}
    {$page_title=__("order_statuses_history")}
    {$order_status_descr = $smarty.const.STATUSES_ORDER|fn_get_simple_statuses:true:true}
    {$order_statuses = $smarty.const.STATUSES_ORDER|fn_get_statuses:$statuses:true:true}

    {if $status_changes}
        <div class="table-responsive-wrapper longtap-selection">
            <table width="100%" class="table table-middle table--relative table-responsive table-manage-orders">
                <thead>
                <tr>
                    <th width="20%">{__("id")}</th>
                    <th width="20%">{__("old_status")}</th>
                    <th width="20%">{__("new_status")}</th>
                    <th width="20%">{__("changed_by")}</th>
                    <th width="20%">{__("changed_at")}</th>
                </tr>
                </thead>
                {foreach from=$status_changes item="o"}
                    <tr>
                        <td width="20%" class="left">
                            {__("order")} #{$o.order_id}
                        </td>
                        <td width="20%" class="left">
                            {$order_statuses[$o.old_status].description}
                        </td>
                        <td width="20%" class="left">
                            {$order_statuses[$o.new_status].description}
                        </td>
                        <td width="20%" class="left">
                            <a href="{fn_url("profiles.update?user_id={$o.user_id}")}">{$o.b_firstname} {$o.b_lastname}</a>
                        </td>
                        <td width="20%" class="left">
                            {$o.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
                        </td>
                    </tr>
                {/foreach}
            </table>
        </div>
    {else}
        <p class="no-items">{__("no_data")}</p>
    {/if}
{/capture}

{include file="common/mainbox.tpl"
title=$page_title
content=$smarty.capture.mainbox
adv_buttons=$smarty.capture.adv_buttons
content_id="view_order_statuses_history"
}
