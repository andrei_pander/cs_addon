<?php

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($mode == 'view') {
    $status_changes = db_get_array("SELECT h.order_id, h.old_status, h.new_status, h.user_id, h.timestamp, u.b_firstname, u.b_lastname FROM ?:order_statuses_history AS h JOIN ?:user_profiles u ON u.user_id = h.user_id ORDER BY h.timestamp DESC");

    Tygh::$app['view']->assign('status_changes', $status_changes);
}
