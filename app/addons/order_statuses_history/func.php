<?php

if ( ! defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_order_statuses_history_change_order_status_post($order_id, $status_to, $status_from, $force_notification, $place_order, $order_info, $edp_data)
{
    return db_query("INSERT INTO ?:order_statuses_history SET ?u", array(
        'order_id' => $order_id,
        'old_status' => $status_from,
        'new_status' => $status_to,
        'user_id' => $_SESSION['auth']['user_id'],
        'timestamp' => TIME,
    ));
}
